#!/usr/bin/env bash

SHELL=/bin/bash

TIMESTAMP=`date +%Y%m%d%H%M%S`

DOCKER_BASE=/swissbib/harvesting/docker.cc
CONFDIR=$DOCKER_BASE/configs
cd $DOCKER_BASE


DEFAULTREPOS="bbfKind bbfPaedlex bbfPaedscr bbfPaedzts ep eraStp eraSupsi stp"


if [ -n "$1" ]; then
  repos=$*
  repo_force=1
else
  repos=${DEFAULTREPOS}
  repo_force=0
fi

for repo in ${repos}; do

  LOCKFILE=${CONFDIR}/${repo}.lock

  if [ -e ${LOCKFILE} ]; then
    echo -n "${repo} is locked, probably by another harvester: "
    cat ${LOCKFILE}
    continue
  else
    echo $$ >${LOCKFILE}
  fi
  
  echo -n "start container for repo: ${repo} at $TIMESTAMP "
  $DOCKER_BASE/admin/start_cc_container.sh $repo >> /swissbib/harvesting/docker.cc/start_cc_container.log
  TIMESTAMP=`date +%Y%m%d%H%M%S`
  echo -n "container for repo: ${repo} finished at $TIMESTAMP "
  rm ${LOCKFILE}

done
