# Data Ingestion

A collection of producers and consumers to feed the Kafka Event Hub.

Deployment script in DeployRep Repo

There is a need to copy manually the configs on the target hosts based on your needs. For example for swisscollections, configs/oai and configs/logs. The oai timestamp is updated in the config file after each harvesting.

The messages are posted to kafka with a kafka header. This kafka header contains a value with the name of the configuration file (for example for ubs.yaml it contains ubs). Then the mongo_feeder reads this value and map it to a Mongo Collection Name (cf. [application.conf in mongo_feeder](https://gitlab.switch.ch/swissbib/swisscollections/mongo_feeder/-/blob/master/src/main/resources/application.conf?ref_type=heads) ). The kafka message key has the same value, uppercase, as a prefix between parentheses. For example (UBS)oai:alma.41SLSP_UBS:9951705570105504.



## How to set up and test

The only external system needed is a kafka cluster. You can do some integration testing manually. In `docker-compose.yml`, you can find a setup for a Kafka Cluster via docker-compose.

How to test the whole system.

Launch the kafka cluster and create the topics

```
make up
```

Run the dataingestion program 
```
python3 run.py --type=OAIProducerKafka --sharedconfig /home/lionel/Documents/mycloud/arbim/git_repo/swisscollections/dataingestion/configs/share/cc.share.yaml /home/lionel/Documents/mycloud/arbim/git_repo/swisscollections/dataingestion/configs/oai/erara.yaml

```

Start a shell to consume the data of the output topic

``` 
./scripts/consume-kafka-topic.sh
```


## How to Deploy

on x2go4 (ssh -A swissbib@ub-x2go4.ub.p.unibas.ch):
```
cd /users/swissbib/deployment/deployRep
./prepareRepo.sh
cd deploy.dataingester
./deploy.dataingester.test.sh
```

## The various consumers

### deletions

Not implemented

### eduplatform

Get data from api

### elastic

Get data from elastic 

### filePush

for nebis originally

one xml file per record

### files

each line is a record

### httpget

not implemented

### mongo

Get data from mongo

### oai

Get data from oai endpoint, this is the main way to get data

### simple

not implemented

### sru

Get data from sru endpoint

### webdav

process multiple records in multiple xml files