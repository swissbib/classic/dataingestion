# coding: utf-8


__author__ = 'swissbib - UB Basel, Switzerland, Guenter Hipler'
__copyright__ = "Copyright 2018, swissbib project"
__credits__ = []
__license__ = "GNU General Public License v3.0"
__maintainer__ = "Guenter Hipler"
__email__ = "guenter.hipler@unibas.ch"
__status__ = "in development"
__description__ = """

                    """

import time

from kafka_event_hub.producers.base_producer import AbstractBaseProducer
from kafka_event_hub.config import OAIConfig
from kafka_event_hub.producers.oai.oai_sickle_wrapper import OaiSickleWrapper

from kafka_event_hub.utility.producer_utility import current_timestamp
from kafka_event_hub.config.config_utility import init_logging

import re

class OAIProducerKafka(AbstractBaseProducer):

    def __init__(self, configrep: str, configrepshare: str):

        AbstractBaseProducer.__init__(self,configrepshare, OAIConfig, configrep)
        logComponenents = init_logging(configrep, self.configuration)
        self._shortcut_source_name = logComponenents['shortcut_source_name']
        self.source_logger_summary = logComponenents['source_logger_summary']
        self.source_logger = logComponenents['source_logger']


    def callback_error(self, exception, key=None):
        self.source_logger.error(str(key, encoding='utf-8'))
        self.source_logger.error(str(exception))
        self.source_logger.error(exception.message)


    def process(self):

        enrich = 1
        numberHarvestedMessages = 0
        try:

            self.source_logger_summary.info('\n\n\n\nStart Harvesting datasource {SOURCE} {STARTTIME}'.format(
                SOURCE=self._shortcut_source_name,
                STARTTIME=current_timestamp()
            ))

            oai_sickle = OaiSickleWrapper(self.configuration,
                                          self.source_logger_summary,
                                          self.source_logger)
            for record in oai_sickle.fetch_iter():
                numberHarvestedMessages += 1

                if self.configuration.use_get_record:
                    try:
                        vlid = self.get_vlid(record.identifier)
                        #mets_record = oai_sickle.get_record(648786, self.configuration.get_record_metadata_prefix)
                        #mets_record = oai_sickle.get_record(20055074, self.configuration.get_record_metadata_prefix)
                        mets_record = oai_sickle.get_record(vlid, self.configuration.get_record_metadata_prefix)

                        # we replace the original dublin core record with the corresponding mets record
                        record = mets_record
                        time.sleep(0.2) # wait 0.2 second between each oai get record call
                    except Exception as baseException:
                        self.source_logger.error('Exception während des GetRecord Harvestingprozesses:  {MESSAGE}'.format(
                            MESSAGE=str(baseException)))
                        continue #we skip this record



                self.send(key=self.createKeyWithPrefix(record.header.identifier).encode('utf8'),
                          message=record.raw.encode('utf8'), headers=[('source',self._shortcut_source_name.encode('utf8'))])

            self.flush()

            self.source_logger_summary.info('Anzahl der nach Kafka gesendeten messages: {ANZAHL}'.format(
                ANZAHL=numberHarvestedMessages
            ))

            self.source_logger_summary.info('STOP Harvesting datasource {SOURCE} {STOPTIME}'.format(
                SOURCE=self._shortcut_source_name,
                STOPTIME=current_timestamp()
            ))

        except Exception as baseException:
            self.source_logger.error('Exception während des Harvestingprozesses:  {MESSAGE}'.format(
                MESSAGE=str(baseException)))
        else:
            self.source_logger_summary.info('Keine Exception im Basisworkflow Harvesting der source {SOURCE}'.format(
                SOURCE=self._shortcut_source_name))
            self.update_configuration(numberHarvestedMessages)

    def createKeyWithPrefix(self, kafkakey):
        return f"({self._shortcut_source_name.upper()}){kafkakey}"

    def update_configuration(self, number_messages = 0):
        self.configuration.update_stop_time()
        if number_messages > 0:
            self.configuration.update_start_time()
        self._configuration.store()

    def get_vlid(self, oai_id):
        # input param: an oai_id as 'oai:www.e-rara.ch:8923269'
        # retrieve VLID from id
        # for example return 8923269
        # if it is not found, return the full oai identifier (works good for bbf scripta paedagogica)
        if oai_id != False:
            id = re.search("[^\d]*(\d+)$", oai_id)
            if id is not None:
                VLID = id.group(1)
                return VLID
            else:
                return oai_id
        else:
            return False

